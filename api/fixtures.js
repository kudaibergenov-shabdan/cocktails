const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Cocktail = require("./models/Cocktail");

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [admin, user] = await User.create({
        email: 'admin@gmail.com',
        password: '123',
        token: nanoid(),
        role: 'admin',
        displayName: 'Admin',
    }, {
        email: 'user@gmail.com',
        password: '123',
        token: nanoid(),
        role: 'user',
        displayName: 'User',
    });

    await Cocktail.create({
        user: admin,
        title: 'Gin',
        image: 'fixtures/gin.jpg',
        published: true,
        rates: [{
            who: admin,
            rate: 5,
        }, {
            who: user,
            rate: 2,
        },]
    }, {
        user: admin,
        title: 'Black Tea',
        image: 'fixtures/black_tea.jpg',
        published: true,
    }, {
        user: admin,
        title: 'Long Island Iced Tea',
        recipe: 'Fill a cocktail shaker with ice. Pour vodka, ' +
            'rum, gin, tequila, triple sec, and sour mix over ice, cover and shake.',
        ingredients: [
            {title: 'white rum', count: '15ml'},
            {title: 'tequila', count: '15ml'},
            {title: 'vodka', count: '15ml'},
            {title: 'triple sec', count: '15ml'}
        ],
        image: 'fixtures/long_island_iced_tea.jpg'
    },{
        user: user,
        title: 'Black Tea',
        image: 'fixtures/black_tea.jpg'
    },{
        user: user,
        title: 'Green Tea',
        image: 'fixtures/green_tea.jpeg',
    },);

    await mongoose.connection.close();
};

run().catch(console.error);