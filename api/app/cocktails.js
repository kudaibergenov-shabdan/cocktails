const express = require('express');
const config = require('../config');
const auth = require("../middleware/auth");
const multer = require("multer");
const {nanoid} = require("nanoid");
const Cocktail = require("../models/Cocktail");
const permit = require("../middleware/permit");
const path = require("path");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', auth, permit('admin', 'user'), async (req, res) => {
    const query = {};
    let cocktails = null;
    try {
        if (req.query.userId) {
            query.user = req.query.userId;
        }

        if (req.query.mine) {
            cocktails = await Cocktail.find({user: req.user}).populate('user', 'displayName');
        } else {
            if (req.user.role === 'admin') {
                cocktails = await Cocktail.find(query).populate('user', 'displayName');
            } else {
                query.published = true
                cocktails = await Cocktail.find(query).populate('user', 'displayName');
            }
        }
        res.send(cocktails);
    } catch (error) {
        res.sendStatus(500);
    }
});

router.get('/private', auth, async (req, res) => {
    const query = {};
    let cocktails = null;
    try {
        if (req.query.userId) {
            query.userId = req.query.userId;
            cocktails = await Cocktail.find({
                user: req.user,
            }).populate('user', 'displayName');
        } else {
            cocktails = await Cocktail.find({
                published: true,
            }).populate('user', 'displayName');
        }
        res.send(cocktails);
    } catch (error) {
        res.sendStatus(500);
    }
});

router.get('/:id', auth, async (req, res) => {
    try {
        const cocktail = await Cocktail.findById(req.params.id);
        if (!cocktail) {
            return res.status(400).send({message: 'Such cocktail has not been found'});
        }
        res.send(cocktail);
    } catch (error) {
        res.status(400).send(error);
    }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
    try {
        const cocktailData = {
            title: req.body.title,
            user: req.user,
            recipe: req.body.recipe,
            ingredients: JSON.parse(req.body.ingredients),
        };

        if (req.file) {
            cocktailData.image = '/uploads' + req.file.filename;
        }

        const cocktail = new Cocktail(cocktailData);

        await cocktail.save();
        res.send(cocktail);
    } catch (error) {
        res.status(400).send(error);
    }
});

router.put('/', auth, async (req, res) => {
    try {
        const query = {};
        if (req.query.cocktailId) {
            query._id = req.query.cocktailId
        }

        const foundCocktail = await Cocktail.findById(query._id);
        if (!foundCocktail) {
            return res.status(400).send({error: 'Cocktail has not been found'});
        }

        if (!foundCocktail.rates) {
            return res.status(400).send({error: 'Cocktail has no rates'});
        }

        if (foundCocktail.rates.length > 0) {
            let modified = false;
            foundCocktail.rates.forEach(rate => {
                if (rate.who.equals(req.user._id)) {
                    rate.rate = req.query.mark
                    modified = true;
                }
            });
            if (!modified) {
                foundCocktail.rates.push({rate: req.query.mark, who: req.user._id});
            }
        } else {
            foundCocktail.rates.push({rate: req.query.mark, who: req.user._id});
        }

        const updatedCocktail = await Cocktail.findByIdAndUpdate(query._id, foundCocktail, {
            new: true
        });
        res.send(updatedCocktail);
    } catch (error) {
        res.sendStatus(500);
    }
});

router.put('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const publishedCocktail = await Cocktail.findByIdAndUpdate(req.params.id, {
            published: true,
        }, {
            new: true
        });
        if (!publishedCocktail) {
            res.send('Can not find. Cocktail has not been updated !');
        }
        res.send(publishedCocktail);
    } catch (error) {
        res.send(error);
    }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const deletedCocktail = await Cocktail.findByIdAndDelete(req.params.id);
        if (!deletedCocktail) {
            res.send('Can not find. Cocktail has not been deleted !');
        }
        res.send(deletedCocktail);
    } catch (error) {
        res.status(400).send(error);
    }
});

module.exports = router;