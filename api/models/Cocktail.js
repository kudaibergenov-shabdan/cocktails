const mongoose = require('mongoose');

const CocktailSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    title: {
        type: String,
        required: true,
        validate: [{
            validator: async function (value) {
                const existed = await Cocktail.findOne({
                    user: this.user,
                    title: value,
                });
                if (existed)
                    return false;
            },
            message: 'You have already created such cocktail !'
        }]
    },
    image: {
        type: String,
        default: null,
    },
    recipe: {
        type: String,
        default: null,
    },
    published: {
        type: Boolean,
        default: false,
    },
    ingredients: [{
        title: String,
        count: String,
    }],
    rates: [{
        who: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        rate: Number,
    }],
});

const Cocktail = mongoose.model('Cocktail', CocktailSchema);
module.exports = Cocktail;