import React from 'react';
import {
    Button,
    Card,
    CardActions,
    CardContent,
    CardMedia,
    Grid,
    IconButton,
    makeStyles,
    Typography
} from "@material-ui/core";
import {apiURL} from "../../config";
import imageNotAvailable from '../../assets/images/not_available.png';
import {Link} from "react-router-dom";
import {Link as LinkMaterial} from "@material-ui/core";
import {sendApproveItem, sendDeleteItem} from "../../store/actions/cocktailsActions";
import {useDispatch, useSelector} from "react-redux";

const useStyles = makeStyles(theme => ({
    card: {
        height: '100%',
    },
    media: {
        textAlign: 'center',
        height: '200px',
    },
    spanHelper: {
        display: 'inline-block',
        height: '100%',
        verticalAlign: 'middle',
    },
    mediaImage: {
        maxWidth: '200px',
        height: 'auto',
        maxHeight: '200px',
        verticalAlign: 'middle',
    },
    iconVisit: {
        color: 'white',
        background: '#3f51b5',
        "&:hover": {
            color: 'blue',
        }
    },
    link: {
        margin: '4px',
    }
}));

const CocktailItem = ({id, title, image, published, author}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    let cardImage = imageNotAvailable;
    if (image) {
        const pathToImage = apiURL + '/';
        cardImage = pathToImage + image;
    }

    const approveItem = (cocktailId) => {
        dispatch(sendApproveItem(cocktailId));
    };

    const deleteItem = (cocktailId) => {
        dispatch(sendDeleteItem(cocktailId));
    };

    return (
        <Grid item xs={12} sm={6} md={4} lg={3}>
            <Card className={classes.card}>
                <CardMedia className={classes.media}>
                    <span className={classes.spanHelper}></span>
                    <img src={cardImage} alt={title} className={classes.mediaImage}/>
                </CardMedia>
                <CardContent>
                    <Typography variant="subtitle1">
                        {title} by
                        <LinkMaterial
                            className={classes.link}
                            to={`/cocktails?userId=${author._id}`}
                            component={Link}
                        >
                            {author.displayName}
                        </LinkMaterial>

                    </Typography>
                </CardContent>
                <CardActions>
                    <IconButton component={Link} to={`/cocktails/${id}`} className={classes.iconVisit}>
                        <Typography variant="caption">Visit</Typography>
                    </IconButton>
                    {
                        user.role === 'admin' &&
                        (<>
                            {
                                !published &&
                                <Button
                                    color="primary"
                                    variant="contained"
                                    onClick={() => approveItem(id)}
                                >
                                    Approve
                                </Button>
                            }
                            <Button
                                color="secondary"
                                variant="contained"
                                onClick={() => deleteItem(id)}
                            >
                                Delete
                            </Button>
                        </>)
                    }
                    <Typography variant="caption">{published ? 'Approved' : 'Not approved'}</Typography>
                </CardActions>
            </Card>
        </Grid>
    );
};

export default CocktailItem;