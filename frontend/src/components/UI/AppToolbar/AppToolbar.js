import React from 'react';
import {AppBar, Button, Grid, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {Link as LinkMaterial} from "@material-ui/core";
import {logoutUser} from "../../../store/actions/usersActions";

const useStyles = makeStyles(theme => ({
    mainLink: {
        color: "inherit",
        textDecoration: 'none',
        '$:hover': {
            color: 'inherit'
        }
    },
    staticToolbar: {
        marginBottom: theme.spacing(2)
    },
    link: {
        color: 'skyblue',
        margin: '2px',
    }
}));

const AppToolbar = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    return (
        <>
            <AppBar position="fixed">
                <Toolbar>
                    <Grid container justifyContent="space-between" alignContent="center">
                        <Grid item>
                            <Typography variant="h6">
                                <Link to="/" className={classes.mainLink}>Photo gallery</Link>
                            </Typography>
                        </Grid>
                        <Grid item>
                            {user
                                ?
                                (
                                    <Grid container spacing={2} alignItems="center">
                                        <Grid item>
                                            Hello, {user.displayName}!
                                            {
                                                <LinkMaterial
                                                    className={classes.link}
                                                    to="/cocktails/mine"
                                                    component={Link}
                                                >
                                                    My cocktails
                                                </LinkMaterial>
                                            }
                                        </Grid>
                                        <Grid item>
                                            <Button
                                                variant="contained"
                                                color="default"
                                                onClick={() => {
                                                    dispatch(logoutUser())
                                                }}
                                            >
                                                Logout
                                            </Button>
                                        </Grid>
                                    </Grid>
                                )
                                :
                                (
                                    <>
                                        <Button component={Link} to="/register" color="inherit">Sign up</Button>
                                        <Button component={Link} to="/login" color="inherit">Sign in</Button>
                                    </>
                                )
                            }
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
            <Toolbar className={classes.staticToolbar}/>
        </>
    );
};

export default AppToolbar;