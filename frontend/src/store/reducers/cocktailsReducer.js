import {
    CLEAR_ERROR_ITEM,
    CREATE_COCKTAIL_ITEM_FAILURE,
    CREATE_COCKTAIL_ITEM_REQUEST, CREATE_COCKTAIL_ITEM_SUCCESS,
    FETCH_FAILURE_COCKTAIL,
    FETCH_FAILURE_COCKTAILS, FETCH_REQUEST_COCKTAIL,
    FETCH_REQUEST_COCKTAILS, FETCH_SUCCESS_COCKTAIL,
    FETCH_SUCCESS_COCKTAILS, ITEM_APPROVED, ITEM_DELETED, SEND_RATE_SUCCESS
} from "../actions/cocktailsActions";

const initialState = {
    cocktails: [],
    fetchLoading: false,
    cocktail: null,
    fetchingSingleCocktail: false,
    userMark: null,
    createItemError: null,
    createItemLoading: false,
};

const cocktailsProducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_REQUEST_COCKTAILS:
            return {...state, fetchLoading: true};
        case FETCH_SUCCESS_COCKTAILS:
            return {...state, fetchLoading: false, cocktails: action.payload};
        case FETCH_FAILURE_COCKTAILS:
            return {...state, fetchLoading: false};
        case FETCH_REQUEST_COCKTAIL:
            return {...state, fetchingSingleCocktail: true};
        case FETCH_SUCCESS_COCKTAIL:
            const cocktail = action.payload.cocktail;
            return {
                ...state,
                fetchingSingleCocktail: false,
                cocktail,
            };
        case FETCH_FAILURE_COCKTAIL:
            return {...state, fetchingSingleCocktail: false};
        case ITEM_APPROVED:
            return {
                ...state,
                cocktails: state.cocktails.map(cocktail => {
                    if (cocktail._id === action.payload) {
                        return {...cocktail, published: true};
                    }
                    return cocktail;
                }),
            };
        case ITEM_DELETED:
            return {
                ...state,
                cocktails: state.cocktails.filter(cocktail => cocktail._id !== action.payload),
            };
        case CREATE_COCKTAIL_ITEM_REQUEST:
            return {...state, createItemLoading: true};
        case CREATE_COCKTAIL_ITEM_SUCCESS:
            return {...state, createItemLoading: false, createItemError: null};
        case CREATE_COCKTAIL_ITEM_FAILURE:
            return {...state, createItemLoading: false, createItemError: action.payload};
        case CLEAR_ERROR_ITEM:
            return {...state, createItemError: null};
        case SEND_RATE_SUCCESS:
            return {
                ...state,
                cocktail: action.payload
            };
        default:
            return state;
    }
};

export default cocktailsProducer;