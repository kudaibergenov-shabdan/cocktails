import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import WarningIcon from '@material-ui/icons/Warning';
import {historyPush} from "./historyActions";

export const FETCH_REQUEST_COCKTAILS = 'FETCH_REQUEST_COCKTAILS';
export const FETCH_SUCCESS_COCKTAILS = 'FETCH_SUCCESS_COCKTAILS';
export const FETCH_FAILURE_COCKTAILS = 'FETCH_FAILURE_COCKTAILS';

export const FETCH_REQUEST_COCKTAIL = 'FETCH_REQUEST_COCKTAIL';
export const FETCH_SUCCESS_COCKTAIL = 'FETCH_SUCCESS_COCKTAIL';
export const FETCH_FAILURE_COCKTAIL = 'FETCH_FAILURE_COCKTAIL';

export const ITEM_APPROVED = 'ITEM_APPROVED';

export const ITEM_DELETED = 'ITEM_DELETED';

export const CREATE_COCKTAIL_ITEM_REQUEST = 'CREATE_COCKTAIL_ITEM_REQUEST';
export const CREATE_COCKTAIL_ITEM_SUCCESS = 'CREATE_COCKTAIL_ITEM_SUCCESS';
export const CREATE_COCKTAIL_ITEM_FAILURE = 'CREATE_COCKTAIL_ITEM_FAILURE';

export const CLEAR_ERROR_ITEM = 'CLEAR_ERROR_ITEM';

export const SEND_RATE_REQUEST = 'SEND_RATE_REQUEST';
export const SEND_RATE_SUCCESS = 'SEND_RATE_SUCCESS';
export const SEND_RATE_FAILURE = 'SEND_RATE_FAILURE';

export const fetchRequestCocktails = () => ({type: FETCH_REQUEST_COCKTAILS});
export const fetchSuccessCocktails = cocktails => ({type: FETCH_SUCCESS_COCKTAILS, payload: cocktails});
export const fetchFailureCocktails = error => ({type: FETCH_FAILURE_COCKTAILS, payload: error});

export const fetchRequestCocktail = () => ({type: FETCH_REQUEST_COCKTAIL});
export const fetchSuccessCocktail = cocktail => ({type: FETCH_SUCCESS_COCKTAIL, payload: cocktail});
export const fetchFailureCocktail = error => ({type: FETCH_FAILURE_COCKTAIL, payload: error});

export const approveItem = cocktailId => ({type: ITEM_APPROVED, payload: cocktailId});

export const deleteItem = cocktailId => ({type: ITEM_DELETED, payload: cocktailId});

export const createCocktailItemRequest = () => ({type: CREATE_COCKTAIL_ITEM_REQUEST});
export const createCocktailItemSuccess = () => ({type: CREATE_COCKTAIL_ITEM_SUCCESS});
export const createCocktailItemFailure = (error) => ({type: CREATE_COCKTAIL_ITEM_FAILURE, payload: error});

export const clearErrorItem = () => ({type: CLEAR_ERROR_ITEM});

export const sendRateRequest = () => ({type: SEND_RATE_REQUEST});
export const sendRateSuccess = (rate) => ({type: SEND_RATE_SUCCESS, payload: rate});
export const sendRateFailure = (error) => ({type: SEND_RATE_FAILURE, payload: error});


export const fetchCocktails = (mine, locationSearch) => {
    return async dispatch => {
        try {
            dispatch(fetchRequestCocktails());
            let response;
            if (mine) {
                response = await axiosApi.get('/cocktails?mine=true');
            } else {
                locationSearch = locationSearch ? locationSearch : '';
                response = await axiosApi.get('/cocktails' + locationSearch);
            }
            dispatch(fetchSuccessCocktails(response.data));
        } catch (error) {
            dispatch(fetchFailureCocktails());
            toast.error('Could not fetch cocktails!', {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    };
};

export const fetchCocktail = (cocktailId) => {
    return async (dispatch, getState) => {
        try {
            dispatch(fetchRequestCocktail());
            const response = await axiosApi.get(`/cocktails/${cocktailId}`);

            const user = getState().users.user;

            dispatch(fetchSuccessCocktail({
                cocktail: response.data,
                loggedUser: user,
            }));
        } catch (error) {
            dispatch(fetchFailureCocktail(error));
        }
    };
};

export const sendApproveItem = cocktailId => {
    return async dispatch => {
        try {
            await axiosApi.put(`/cocktails/${cocktailId}`);
            dispatch(approveItem(cocktailId));
            toast.success('Cocktail successfully updated', {
                position: toast.POSITION.BOTTOM_RIGHT,
            });
        } catch (error) {
            toast.error('Error! Cannot update cocktail!', {
                position: toast.POSITION.BOTTOM_RIGHT,
            });
        }
    };
};

export const sendDeleteItem = cocktailId => {
    return async dispatch => {
        try {
            await axiosApi.delete(`/cocktails/${cocktailId}`);
            dispatch(deleteItem(cocktailId));
            toast.success('Cocktail successfully deleted', {
                position: toast.POSITION.BOTTOM_RIGHT,
            });
        } catch (error) {
            toast.error('Error! Cannot delete cocktail!', {
                position: toast.POSITION.BOTTOM_RIGHT,
            });
        }
    };
};

export const createCocktail = cocktail => {
    return async dispatch => {
        try {
            dispatch(createCocktailItemRequest());
            await axiosApi.post('/cocktails', cocktail);
            dispatch(createCocktailItemSuccess());
            dispatch(historyPush('/'));
            toast.success('Cocktail successfully created', {
                position: toast.POSITION.BOTTOM_RIGHT,
            });
        } catch (error) {
            dispatch(createCocktailItemFailure(error.response.data));
            toast.error('Error! Cocktail has not been created!', {
                position: toast.POSITION.BOTTOM_RIGHT,
            });
        }
    };
};

export const sendRate = (cocktail, mark) => {
    return async dispatch => {
        try {
            dispatch(sendRateRequest());
            const response = await axiosApi.put(`/cocktails?cocktailId=${cocktail._id}&mark=${mark}`);
            dispatch(sendRateSuccess(response.data));
        }
        catch (error) {
            dispatch(sendRateFailure(error));
            toast.error('Rate has not been updated!', {
                position: toast.POSITION.BOTTOM_RIGHT,
            });
        }
    };
}