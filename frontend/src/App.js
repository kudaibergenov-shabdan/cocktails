import {Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import Cocktails from "./containers/Cocktails/Cocktails";
import Cocktail from "./containers/Cocktail/Cocktail";
import AddCocktail from "./containers/AddCocktail/AddCocktail";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact render={() => {return <Cocktails mine={false}/>}}/>
            <Route path="/cocktails" exact render={() => {return <Cocktails mine={false}/>}} />
            <Route path="/cocktails/mine" render={() => {return <Cocktails mine={true}/>}}/>
            <Route path="/cocktails/new" exact component={AddCocktail}/>
            <Route path="/cocktails/:id" exact component={Cocktail}/>
            <Route path="/login" component={Login}/>
            <Route path="/register" component={Register}/>
        </Switch>
    </Layout>
);

export default App;
