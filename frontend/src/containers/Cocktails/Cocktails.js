import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link, Redirect, useLocation} from "react-router-dom";
import {fetchCocktails} from "../../store/actions/cocktailsActions";
import {Button, CircularProgress, Grid, Typography} from "@material-ui/core";
import CocktailItem from "../../components/CocktailItem/CocktailItem";

const Cocktails = ({mine}) => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const cocktails = useSelector(state => state.cocktails.cocktails);
    const fetchLoading = useSelector(state => state.cocktails.fetchLoading);
    const location = useLocation();
    useEffect(() => {
        dispatch(fetchCocktails(mine, location.search));
    }, [dispatch, mine, location.search]);

    if (!user) {
        return <Redirect to="/login"/>
    }

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">
                <Grid item>
                    {
                        (mine)
                            ?
                            <Typography variant="h4">My Cocktails</Typography>
                            :
                            (user.role === 'admin')
                                ?
                                <Typography variant="h4">
                                    All Cocktails
                                </Typography>
                                :
                                <Typography variant="h4">Cocktails approved by admin</Typography>
                    }
                </Grid>
                <Grid item>
                    <Button
                        color="primary"
                        variant="contained"
                        component={Link}
                        to="/cocktails/new"
                    >Add
                    </Button>
                </Grid>
            </Grid>
            <Grid item>
                <Grid item container justifyContent="center" direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : (
                        cocktails.map(cocktailItem => (
                            <CocktailItem
                                key={cocktailItem._id}
                                id={cocktailItem._id}
                                title={cocktailItem.title}
                                image={cocktailItem.image}
                                author={cocktailItem.user}
                                recipe={cocktailItem.recipe}
                                published={cocktailItem.published}
                                ingredients={cocktailItem.ingredients}
                                rates={cocktailItem.rates}
                            />
                        ))
                    )}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Cocktails;