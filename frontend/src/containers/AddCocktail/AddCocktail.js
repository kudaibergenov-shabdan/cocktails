import React, {useEffect, useState} from 'react';
import {Button, Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {clearErrorItem, createCocktail} from "../../store/actions/cocktailsActions";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2),
    },
    btnAdd: {
        margin: '5px',
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const AddCocktail = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.cocktails.createItemError);
    const loading = useSelector(state => state.cocktails.createItemLoading);

    const [state, setState] = useState({
        title: '',
        recipe: '',
        ingredients: [{title: '', count: ''}],
        image: null,
    });

    useEffect(() => {
        return () => {
            dispatch(clearErrorItem());
        };
    }, [dispatch]);

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const ingredientChangeHandler = (e, index) => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {
                ...prevState,
                ingredients: prevState.ingredients.map((ingredient, idx) => {
                    if (idx === index) {
                        return {...ingredient, [name]: value};
                    }
                    return ingredient;
                })
            }
        });
    };

    const createIngredientItem = () => {
        setState(prevState => {
            return {
                ...prevState,
                ingredients: [...prevState.ingredients, {title: '', count: ''}],
            }
        })
    };

    const deleteIngredientItem = (index) => {
        setState(prevState => {
            return {
                ...prevState,
                ingredients: prevState.ingredients.filter(
                    (ingredient, idx) => idx !== index
                ),
            }
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {...prevState, [name]: file};
        });
    };

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(state).forEach(key => {
            const ingredientsFieldName = 'ingredients';
            if (key !== ingredientsFieldName) {
                formData.append(key, state[key]);
            } else {
                formData.append(ingredientsFieldName, JSON.stringify(state.ingredients));
            }
        });
        dispatch(createCocktail(formData));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <>
            <Typography variant="h4">New cocktail</Typography>
            <Grid
                container
                direction="column"
                spacing={2}
                component="form"
                className={classes.root}
                autoComplete="off"
                onSubmit={submitFormHandler}
            >
                <Grid item xs>
                    <TextField
                        variant="outlined"
                        label="Title"
                        name="title"
                        value={state.title}
                        onChange={inputChangeHandler}
                        error={Boolean(getFieldError('title'))}
                        helperText={getFieldError('title')}
                    />
                </Grid>
                <Grid item xs>
                    <TextField
                        variant="outlined"
                        label="Recipe"
                        name="recipe"
                        value={state.recipe}
                        onChange={inputChangeHandler}
                        error={Boolean(getFieldError('recipe'))}
                        helperText={getFieldError('recipe')}
                        multiline
                        rows={5}
                    />
                </Grid>
                <Grid item xs>
                    {state.ingredients.map((ingredient, index) => (
                        <Grid container direction="row" key={index} spacing={2} alignItems="center">
                            <Grid item xs={5}>
                                <TextField
                                    variant="outlined"
                                    label="Ingredient title"
                                    name="title"
                                    value={ingredient.title}
                                    onChange={e => ingredientChangeHandler(e, index)}
                                />
                            </Grid>
                            <Grid item xs={5}>
                                <TextField
                                    variant="outlined"
                                    label="Ingredient count"
                                    name="count"
                                    value={ingredient.count}
                                    onChange={e => ingredientChangeHandler(e, index)}
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <Button
                                    color="secondary"
                                    variant="contained"
                                    onClick={() => deleteIngredientItem(index)}
                                >
                                    Delete
                                </Button>
                            </Grid>
                        </Grid>
                    ))}
                    <Grid container item justifyContent="center">
                        <Button
                            className={classes.btnAdd}
                            variant="contained"
                            color="default"
                            onClick={createIngredientItem}
                        >
                            Add
                        </Button>
                    </Grid>
                </Grid>
                <Grid item xs>
                    <TextField
                        type="file"
                        name="image"
                        onChange={fileChangeHandler}
                        error={Boolean(getFieldError('image'))}
                        helperText={getFieldError('image')}
                    />
                </Grid>
                <Grid item xs>
                    <ButtonWithProgress
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        loading={loading}
                        disabled={loading}
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </>
    );
};

export default AddCocktail;