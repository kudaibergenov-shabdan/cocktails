import React, {useEffect} from 'react';
import {Button, CircularProgress, Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchCocktail, sendRate} from "../../store/actions/cocktailsActions";
import {apiURL} from "../../config";
import imageNotAvailable from '../../assets/images/not_available.png';

const useStyles = makeStyles(theme => ({
    mediaImage: {
        width: '100%',
        maxWidth: '400px',
        height: 'auto',
    },
    subtitle: {
        margin: '0 10px',
    },
}));

const Cocktail = ({match}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const cocktail = useSelector(state => state.cocktails.cocktail);
    const fetchingSingleCocktail = useSelector(state => state.fetchingSingleCocktail);
    const user = useSelector(state => state.users.user);

    let cardImage = imageNotAvailable;
    if (cocktail && cocktail.image) {
        const pathToImage = apiURL + '/';
        cardImage = pathToImage + cocktail.image;
    }

    let userMark = (user && cocktail && cocktail.rates && cocktail.rates.length > 0) ?
        cocktail.rates.filter(rate => rate.who === user._id)[0]:
        null;

    useEffect(async () => {
        dispatch(fetchCocktail(match.params.id));
    }, [match.params.id]);

    const sendMark = e => {
        const mark = e.currentTarget.value;
        dispatch(sendRate(cocktail, mark));
    }

    return (
        <Grid container direction="column" spacing={2}>
            {
                !cocktail
                    ?
                    <Typography>Nothing found</Typography>
                    :
                    fetchingSingleCocktail
                        ? (
                            <Grid container justifyContent="center" alignItems="center">
                                <Grid item>
                                    <CircularProgress/>
                                </Grid>
                            </Grid>
                        )
                        : (
                            <Grid item xs={12}>
                                <Grid container direction="row" spacing={2}>
                                    <Grid item xs={6}>
                                        <img src={cardImage} alt={cocktail.title} className={classes.mediaImage}/>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Typography variant="h6">{cocktail.title}</Typography>
                                        <Grid item>Ingredients:
                                            {cocktail.ingredients.length > 0 ?
                                                (
                                                    <Grid container direction="column">
                                                        {cocktail.ingredients.map((ingredient, index) => (
                                                            <Grid item key={index}>
                                                                <Typography variant="subtitle2"
                                                                            className={classes.subtitle}>
                                                                    {ingredient.title} - {ingredient.count}
                                                                </Typography>
                                                            </Grid>
                                                        ))}
                                                    </Grid>
                                                ) :
                                                'No Info'
                                            }
                                        </Grid>
                                        <Grid item>Recipe:
                                            {cocktail.recipe ? (
                                                <Typography variant="subtitle2" className={classes.subtitle}>
                                                    {cocktail.recipe}
                                                </Typography>
                                            ) : (
                                                'No info'
                                            )}
                                        </Grid>
                                        <Grid item>
                                            Rating:
                                            {cocktail.rates.length > 0 ? (
                                                <>
                                                    <Typography variant="subtitle2" className={classes.subtitle}>
                                                        Rates count: {cocktail.rates.length}
                                                    </Typography>
                                                    <Typography variant="subtitle2" className={classes.subtitle}>
                                                        Average:
                                                        {
                                                            (
                                                                cocktail.rates.reduce(
                                                                    (acc, rate) => acc + rate.rate, 0
                                                                ) / cocktail.rates.length
                                                            ).toFixed(1)
                                                        }
                                                    </Typography>
                                                </>
                                            ) : (
                                                'No info'
                                            )}
                                        </Grid>
                                        <Grid container direction="row">
                                            <Grid item>
                                                <Button
                                                    variant="contained"
                                                    value={1}
                                                    onClick={sendMark}
                                                >1</Button>
                                            </Grid>
                                            <Grid item>
                                                <Button
                                                    variant="contained"
                                                    value={2}
                                                    onClick={sendMark}
                                                >2</Button>
                                            </Grid>
                                            <Grid item>
                                                <Button
                                                    variant="contained"
                                                    value={3}
                                                    onClick={sendMark}
                                                >3</Button>
                                            </Grid>
                                            <Grid item>
                                                <Button
                                                    variant="contained"
                                                    value={4}
                                                    onClick={sendMark}
                                                >4</Button>
                                            </Grid>
                                            <Grid item>
                                                <Button
                                                    variant="contained"
                                                    value={5}
                                                    onClick={sendMark}
                                                >5</Button>
                                            </Grid>
                                        </Grid>
                                        <Grid item>
                                            {
                                                userMark
                                                ? ( 'Your marks is: ' + userMark.rate)
                                                : ('Please, send a rate to this cocktail !')
                                            }
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        )
            }
        </Grid>
    );
};

export default Cocktail;